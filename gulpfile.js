var gulp = require('gulp');
var replace = require('gulp-replace');
var vulcanize = require('gulp-vulcanize');
var crisper = require('gulp-crisper');
var connect = require('gulp-connect');
var shell = require('gulp-shell');
var minifyInline = require('gulp-minify-inline');

gulp.task('build', function() {
    gulp.src('temp/5FB912C4-4931-40C9-81CC-BE7E29DC78D9/*')
        .pipe(gulp.dest('dist/5FB912C4-4931-40C9-81CC-BE7E29DC78D9'));

    return gulp.src('campus-course-student-performance.html')
        .pipe(replace(/<link rel="import" href="../g, '<link rel="import" href="../../bower_components'))
        .pipe(gulp.dest('dist/5FB912C4-4931-40C9-81CC-BE7E29DC78D9'))
        .pipe(replace(/<script src="../g, '<script src="../../bower_components'))
        .pipe(gulp.dest('dist/5FB912C4-4931-40C9-81CC-BE7E29DC78D9'))
        .pipe(replace(/campus-behavior.html/g, 'campus-develope.html'))
        .pipe(gulp.dest('dist/5FB912C4-4931-40C9-81CC-BE7E29DC78D9'))
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('dist/5FB912C4-4931-40C9-81CC-BE7E29DC78D9'));
});

gulp.task('serve', function() {
    connect.server({
        root: 'dist'
    });
});

gulp.task('default', ['serve', 'build'], shell.task([
    /^win/.test(require('os').platform()) ? 'start http://localhost:8080/' : 'open http://localhost:8080/'
]));
